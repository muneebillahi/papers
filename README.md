# ![papers-logo] Papers

Papers is a document viewer capable of displaying multiple and single
page document formats like PDF and Postscript.  For more general
information about Papers and how to get started, please visit
https://welcome.gnome.org/app/Papers

If you are a developer, make sure to read the [contributing](CONTRIBUTING.md)
guidelines before starting to work on any changes.

This software is licensed under the [GPLv2][license].

[![flatpak]](https://flathub.org/apps/details/org.gnome.Papers)

## Papers Requirements

* [GNOME Platform libraries][gnome]
* [Poppler for PDF viewing][poppler]

## Papers Optional Backend Libraries

* [Spectre for PostScript (PS) viewing][ghostscript]
* [DjVuLibre for DjVu viewing][djvulibre]
* [Archive library for Comic Book Resources (CBR) viewing][comics]
* [LibTiff for Multipage TIFF viewing][tiff]
* [LibGXPS for XML Paper Specification (XPS) viewing][xps]

[gnome]: https://www.gnome.org/
[poppler]: https://poppler.freedesktop.org/
[ghostscript]: https://www.freedesktop.org/wiki/Software/libspectre/
[djvulibre]: https://djvulibre.djvuzone.org/
[comics]: https://libarchive.org/
[tiff]: http://libtiff.org/
[xps]: https://wiki.gnome.org/Projects/libgxps
[license]: COPYING
[papers-logo]: data/icons/scalable/apps/org.gnome.Papers.svg
[flatpak]: https://flathub.org/api/badge?svg&locale=en

### Code of Conduct

When interacting with the project, the [GNOME Code Of Conduct](https://conduct.gnome.org/) applies.
