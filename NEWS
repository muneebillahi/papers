Papers 47.beta
---------------

Papers has seem mostly changes in the libraries and refactorings since 47.alpha
For packagers:

* We now require libadwaita 1.6.beta and GTK 4.15.2
* We have changed the flatpak permissions to read local files. This is necessary
to reload the document on changes regardless of the sandbox type in use
* The "introspection" configure option is now a feature instead of a boolean
* The "gtk_doc" configure option has been renamed to "documentation"

Other changes we can highlight:

libppsview:
* The gesture management of the document view has been heavily refactored.
Previously, the gesture tracking was done manually, while we are now heavily
relying on GTK4's gesture handling. This makes the experience on touchscreens
a lot more polished
* Drag and dropping into the view is no longer supported

shell:
* The Escape key now works with most dialogs
* Implement modern mockups for the annotations sidebar
* Implement changing color of highlight annotation on creation
* Modify default yellow color for annotations
* Use AdwSpinner instead of GtkSpinner where appropriate


Papers 47.alpha
---------------

Papers has continued with a strong development pace. Most remarkable changes
since the previous release is that the UI now fits on narrow screens! For
packagers:

* We now require libadwaita 1.5.0 and GTK 4.15.1

In addition to a multitude of bug fixes and refactors, we can highlight:

libppsview:
* PpsView does no longer send a signal on annotation change. Instead consumers
  are expected to use the properties of the annotation itself

shell:
* Split the header bar in two according to mockups
* Place the search locally into the sidebar instead of having a specific
  toolbar
* Remove the zoom selector and add instead zoom overlay buttons
* Remove dbus interface for PpsWindow, since we have no use for it anymore.
  There is now only the interface for PpsApplication, as we are looking into
  a future with a single application instance
* Ported several widgets to Rust
* Use AppStream data to build the about dialog
* Add "Open With" action to open the document with an alternative application
* Port multiple dialogs to AdwDialog


Papers 46.1
--------------
* Update flatpak dependencies and manifest name
* Fix gtk requirement in packageconfig files


Papers 46.0
---------------

Papers has been forked from Evince, including the whole history of the
evince-next branch to which the authors of the fork contributed extensively.
Papers has landed the GTK4 port from Qiu Wenbo, many (breaking) cleanups to
both libraries, and a bunch of cleanups, modernizations and improvements to
the application itself. For packagers:

* The build now requires rust
* synctex support has been dropped (possibly temporarily)
* DVI backend has been removed

As of the changes in general, we can highlight:

libraries (libppsdocument, libppsview):
  * Due to the of modernization of these, all deprecated API has been removed.
    There have also been some API-breaking changes all around the code, mostly
    aimed at implementing new patterns. The goal of the authors is to reduce the
    public surface of the libraries. So please come in contact if you are a user
    and want to continue using the libraries long-term. We want to learn from
    your usage to provide a good and long-term maintainable experience.
  * The libraries no longer provide any icons or use any custom ones.
  * EvDocumentModel dual-page property has been removed
  * Transition backends to use a new interface for registration
  * Mark types as derivable or final as need be
  * Rename and deprecate EvJobs that do rendering based on cairo
  * Drop EvJobLoadStream and EvJobLoadGFile. Merge EvJobLoad and EvJobLoadFd
  * Some work on sanity of PpsJobs API.
  * GPU based transition of presentation mode with additional effects support.
    Transitions will no longer work if HW acceleration is not supported
  * Use GThreadPool for PpsJobScheduler. Many jobs could now run concurrently
    (even though they don't yet do to locking issues (see #107) ), and
    we no longer leak the thread memory.
  * PPS_PROFILE and PPS_PROFILE_JOBS environment variables for profiling are now
    ignored. Anybody wishing to profile papers can now use sysprof, which
    provides a far better interface.
  * Use new GtkFileDialog API. As a future goal it is considered to drop the GTK
    dependency of libppsdocument
  * Rename pps_job_is_failed to pps_job_is_succeeded
  * PpsAnnotation fields are now private and not part of the API. Please use
    getters and setters to interact with them.

shell:
  * Port to GTK4 thanks to the effort of Qiu Wenbo
  * Removed spell-checking feature. Hopefully will come back through libspelling
  * Implemented some new mockups for the first time in years (thanks Chris!)
  * Lots of small papercuts from the GTK4 migration and other widgets that had
    not received love in a while.
  * Many internal cleanups to use standard widgets, instead of showing and
    hiding internal widgets as different parts of the application change.
  * Ported many widgets to UI templates
  * Make the main widget of PpsWindow an AdwViewStack
  * Start rewriting the UI in Rust, and port several widgets (#21)
  * Rework a big part of the shell with modern widgetry.
  * Fix dbus names to fit app id.
  * Drop the auto-scroll feature for maintainability reasons.
  * Drop papersd daemon. This was an optional feature that took care that
    would only be an instance of every document opened. So for example, clicking
    on an already opened document in Nautilus would move focus to the opened document
    instead of opening a new one. With this change, new documents will be opened
    unconditionally. We plan to bring the old feature back by refactoring some of
    the code, but it requires better isolation of document rendering, so it will
    probably not be available in the next release
  * Remove lots of actions on PpsWindow, and start splitting some of them up
    into a new "doc" action group. Ideally, in the future those will be used
    for a new PpsDocumentView widget.
  * Remove user reload action, documents will be reloaded automatically if
    changes are detected.
  * Warn users about broken metadata
  * Remove annotation toolbar, and only allow creating annotations through
    shortcuts and the context menu.
  * Show a loading view when the document takes long to load, which greatly
    improves the UX.
  * Introduce PpsSearchContext to hold the search, splitting the logic from
    PpsWindow, and getting another step closer to having the logic ready for a
    tabbed window.

properties:
  * Can now be used in Nautilus again. Dropped GTK dependency

backends:
  * Dropped support for DVI file format. This is an old and seldomly used
    document format. The backend has lived unmaintained for many years, and
    we don't have the bandwidth or interest to maintain it. DVI documents
    can always be converted to PDF using Evince or other programs. If anybody
    in the community has a strong interest, they could always maintain an
    OOT backend, or come to us.
  * Dropped synctex support. This was planned to work together with gedit,
    that is no longer a Core App. And modern tex editors all have a PDF
    previewer built-in. So there's no need to duplicate that work when we
    cannot maintain a copy of the synctex library internally.
  * Moved under libdocument

Of course, all this does not come without some issues, so we have likely
regressed in some aspects. We look forward to testers and gathering feedback.

As of the writing, the total diff lays at:

git diff -M -B --stat  fea2b4a8f HEAD -- . ':(exclude)shell-rs/ev-girs/*.gir' ':(exclude)po'
781 files changed, 73778 insertions(+), 107599 deletions(-)
